﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

using RPL_DataAccess_Library.Data;
using RPL_DataAccess_Library.Repositories;
using RPL_DataAccess_Library;
using RPL_OrdersDesktop_Client.Models;

namespace RPL_OrdersDesktop_Client.Services
{
    public class OrdersService : IServices<OrdersModel>
    {
        public ObservableCollection<OrdersModel> ItemData { get; set; } = new ObservableCollection<OrdersModel>();
        public List<OrdersModel> Items { get; set; } = new List<OrdersModel>();

        private readonly Repository<Orders> repo = new OrdersRepository();
        private readonly OrdersModel model = new OrdersModel();

        public async Task<bool> Event_ReadAsync(List<OrdersModel> item)
        {
            try
            {
                foreach (var value in JArray.Parse(repo.DataSerialized))
                {
                    var json = JObject.Parse(value.ToString());
                    item.Add(model.Entity(json));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Items = item;
            return await Task.FromResult(true);
        }

        public async Task<bool> Event_ReadAsync()
        {
            Items = new List<OrdersModel>();

            try
            {
                foreach (var value in JArray.Parse(repo.DataSerialized))
                {
                    var json = JObject.Parse(value.ToString());
                    Items.Add(model.Entity(json));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                ItemData.Clear();
                foreach (var item in Items)
                {
                    ItemData.Add(item);
                }
            }
            return await Task.FromResult(true);
        }

        public async Task Event_CreateAsync(OrdersModel model)
        {
            repo.Model = model;
            repo.DataSerialized = ((int)DataVerbs.Post).ToString();
            await Event_ReadAsync();
        }

        public async Task Event_UpdateAsync(OrdersModel model)
        {
            repo.Model = model;
            repo.DataSerialized = ((int)DataVerbs.Put).ToString();
            await Event_ReadAsync();
        }

        public async Task Event_DeleteAsync(OrdersModel model)
        {
            var msg = MessageBox.Show("Are You Sure?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (msg == MessageBoxResult.Yes)
            {
                repo.Model = model;
                repo.DataSerialized = ((int)DataVerbs.Delete).ToString();
                await Event_ReadAsync();
            }
        }
    }
}
